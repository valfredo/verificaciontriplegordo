import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import ParMillonario from '../views/ParMillonario';
import TripleTerminal from '../views/TripleTerminal';
import SuperDupleta from '../views/SuperDupleta';
import EscanearCodigo from '../views/EscanearCodigo';

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Par Millonario" component={ParMillonario} />
      <Drawer.Screen name="Triple Terminal" component={TripleTerminal} />
      <Drawer.Screen name="Super Dupleta" component={SuperDupleta} />
      <Drawer.Screen name="Instantaneo" component={EscanearCodigo} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
