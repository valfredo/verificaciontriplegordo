import React from 'react';
import EscanearCodigo from '../views/EscanearCodigo';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// import VerificarTicket from '../views/VerificarTicket';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Escanear" component={EscanearCodigo} />
      {/* <Stack.Screen name="VerificarTicket" component={VerificarTicket} />  */}
    </Stack.Navigator>
  );
};

export default MainStack;
