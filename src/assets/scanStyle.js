import { Dimensions, Platform } from 'react-native';
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
const DeviceWidth = Dimensions.get('window').width;

// CONSTANTES PARA EL ESCANER
const SCREEN_WIDTH = Dimensions.get("window").width;
const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency

const rectDimensions = SCREEN_WIDTH * 0.65; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.005; // this is equivalent to 2 from a 393 device width
const rectBorderColor = "#fff";

const scanBarWidth = SCREEN_WIDTH * 0.46; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const scanBarColor = "red";  


var { height } = Dimensions.get('window');
 
var box_count = 3;
var box_height = height / box_count;

const styles = {
  sharedGridStyles: {
    width: SCREEN_WIDTH * 0.19,
    height: SCREEN_WIDTH * 0.15,
    backgroundColor: "transparent",
    borderColor: "#0a0d64",
    borderLeftWidth: 4,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    flex: 1,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    borderTopWidth: 1,
  },
  sharedGridStylesCenter: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
  },
  sharedGridStylesRight: {
    borderRightWidth: 4,
    borderLeftWidth: 1,
  },
  sharedGridTextStyles: {
    width: SCREEN_WIDTH * 0.19,
    height: SCREEN_WIDTH * 0.19,
    backgroundColor: "transparent",
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center"
  },
  sharedGridEmptyStyles: {
    width: SCREEN_WIDTH * 0.2,
    height: SCREEN_WIDTH * 0.19,
    backgroundColor: 'transparent',
  },
  sharedTripleAbiertoStyles: {
    width: SCREEN_WIDTH * 0.15,
    height: SCREEN_WIDTH * 0.15,
    marginBottom: 1,
    backgroundColor: "transparent",
    borderColor: "red",
    borderBottomWidth: 4,
    borderTopWidth: 4,
    flex: 1,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center"
  },
  cardView: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scanCardView: {
    width: deviceWidth - 32,
    height: deviceHeight / 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: 'white',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
  },

  //MODAL
  contenedor: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },

  // BASE
  scrollViewStyle: {
    flex: 1,
    backgroundColor: '#00000000',
  },
  // Headers
  header: {
    backgroundColor: '#0a0d64',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
  },

  tinyLogo: {
    width: 210,
    height: 80,
  },
  tinyLogoMontos: {
    width: 10,
    height: 10,
  },
  tinyLogoRedes: {
    width: 30,
    height: 30,
  },
  // Seccion texto
  colorfondoTexto: {
    backgroundColor: '#F8CC23',
  },
  colorfondoTexto2: {
    backgroundColor: '#F8CC23',
    width: "50%",
    alignSelf: 'center',
    borderRadius: 50
  },
  textTitle: {
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'center',
    padding: 8,
    color: '#0a0d64',
  },
  textTitle1: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#0a0d64',
  },
    textTitle2: {
      fontWeight: 'bold',
      fontSize: 20,
      textAlign: 'center',
      padding: 10,
      color: '#fff',
  },
  fecha: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#0a0d64',
  },
  textoMensaje: {
    fontSize: 40,
    fontWeight: 'bold',
  },

  //Cuadricula
  borderCuariculas: {
    flexDirection: 'row',
    marginTop: Platform.OS === 'android' ? 0 : 30,
  },

  estiloNumeros: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#000',
  },

  montosPremios: {
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  esitloMontos: {
    fontSize: 20,
    fontWeight: '900',
    color: '#0a0d64',
  },
  esitloMontos2: {
    fontSize: 25,
    fontWeight: '900',
    color: '#0a0d64',
  },
  esitloMontos3: {
    fontSize: 15,
    fontWeight: '900',
    color: '#0a0d64',
  },
  esitloMontos4: {
    fontSize: 10,
    fontWeight: '900',
    color: '#0a0d64',
  },
  esitloMontos5: {
    fontSize: 12,
    fontWeight: '900',
    color: '#0a0d64',
  },

  // BOTON
  alignBoton: {
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: Platform.OS === 'android' ? 0 : 30,
  },

   alignBotonInicio: {
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: Platform.OS === 'android' ? 40 : 30,
  },

  buttonTouchable: {
    fontSize: 25,
    backgroundColor: '#0a0d64',
    marginBottom: Platform.OS === 'android' ? 15 : 0,

    width: deviceWidth - 200,
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    borderRadius: 50,
  },
  
  buttonTouchableYellow: {
    fontSize: 25,
    backgroundColor: '#F8CC23',
    marginBottom: Platform.OS === 'android' ? 15 : 0,

    width: deviceWidth - 200,
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    borderRadius: 50,
  },

  btnSalir: {
    fontSize: 25,
    backgroundColor: '#0a0d64',
    marginBottom: 30,
    width: deviceWidth - 100,
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    borderRadius: 50,
  },

  buttonTextStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  buttonTextStyleBlue: {
    color: '#0a0d64',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  borderTripleInstantaneo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  borderTripleInstantaneoNuevo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  //footer
  borderTriple: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    backgroundColor: '#0a0d64',
    alignItems: 'center',
    height: 50,
  },
  borderFooter: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
  },

  // ESTILOS PARA EL ESCANER
  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: 'center',
    alignItems: 'center',
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25,
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor,
  },

  // VERIFICAION DE TICKET
  textPasos: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#0a0d64',
  },
  infoPasos: {
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'center',
    color: '#0a0d64',
    // marginBottom: 5,
  },
  buttonPasos: {
    backgroundColor: '#0a0d64',
    marginBottom: Platform.OS === 'android' ? 15 : 0,

    width: deviceWidth - 200,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderRadius: 50,
    margin: 5,
  },

  divBtnPasos: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  buttonProgress: {
    backgroundColor: '#F8CC23',
    marginBottom: Platform.OS === 'android' ? 15 : 0,
    width: deviceWidth - 280,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    borderRadius: 50,
    fontSize: 15,
    color: '#0a0d64',
  },
  progressColor: {
    backgroundColor: '#0a0d64',
  },
  contentComponent: {
    marginTop: 0,
  },
  stepperStyle: {
    backgroundColor: '#0a0d64',
    width: 30,
    height: 30,
    borderRadius: 30,
    borderColor: '#0a0d64',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mensajeError: {
    fontSize: 15,
    color: 'red',
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
  },
  tittlePremios: {
    textAlign: 'center',
    // alignSelf: 'center',
    marginBottom: 0,
    color: '#0a0d64',
    fontWeight: '900',
    fontSize: 45,
  },
  tittleDolares: {
    textAlign: 'center',
    // alignSelf: 'center',
    marginTop: -15,
    marginBottom: 10,
    color: '#000',
    fontWeight: '900',
    fontSize: 20,
  },
  tittlePremios2: {
    textAlign: 'center',
    // alignSelf: 'center',
    marginBottom: 0,
    color: '#0a0d64',
    fontWeight: '900',
    fontSize: 25,
  },
  tittleDolares2: {
    textAlign: 'center',
    // alignSelf: 'center',
    marginTop: -5,
    marginBottom: 10,
    color: '#000',
    fontWeight: '900',
    fontSize: 15,
  },
  stepperColor: {
    textAlign: 'center',
    padding: 10,
    backgroundColor: '#F8CC23',
    borderRadius: 20,
  },
  stepperTextColor:{
    color: '#0a0d64', fontSize: 15, fontWeight: '900',
  },
  loadingCenter: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    zIndex: 999, // works on ios
    elevation: 999,
    backgroundColor: 'rgba(227, 227, 227, 0.8)',
  },
  separadorSinColor: {marginTop: 10, marginBottom: 10},
  separadorColor: {marginTop: 10, marginBottom: 10, borderBottomWidth: 3, borderColor: '#dde9f5'},
  separadorColor2: {marginTop: 5, borderBottomWidth: 3, borderColor: '#dde9f5'},
  separadorColor3: {marginTop: 15, borderBottomWidth: 3, borderColor: '#dde9f5'}
};
export default styles;