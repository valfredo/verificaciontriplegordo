import React, {Fragment, useState, useEffect} from 'react';
import Header from '../components/Header/Header';
import SocialNetworks from '../components/SocialNetworks/SocialNetworks';
import TituloText from '../components/TituloText/TituloText';

import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from '../assets/scanStyle';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Modal,
  Alert,
  ScrollView,
  Dimensions,
  FlatList, 
  ActivityIndicator,
  Button,
  Platform
} from 'react-native';

import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import RNTextDetector from 'rn-text-detector';

import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';
import base64 from 'react-native-base64';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

const VerificarTicket = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [dataResult, setDataResult] = useState({});
  const [responseLoading, setResponseLoading] = useState(false);

  const[sinEspacio, setSinEspacio] = useState("");
  const[noSorteo, setNoSorteo] = useState("");
  const[validacionSerial, setValidacionSerial] = useState("");
  const[validarCodigo,setValidarCodigo] = useState("");
  const[validarCodigoQR, setValidarCodigoQR] = useState("");
  const[nuevaMatriz,setNuevaMatriz] = useState([]);

  const sliceNoSorteo = noSorteo;
  console.log(sliceNoSorteo)

  const [active, setActive] = useState(0);

  const encodeSinEspacio = sinEspacio ? base64.encode(sinEspacio) : "";
  console.log("encodeSinEspacio",encodeSinEspacio,sinEspacio);

  const encodeNoSorteo = sliceNoSorteo ? base64.encode(sliceNoSorteo) : "";
  console.log("encodeNoSorteo",encodeNoSorteo,sliceNoSorteo);

  const encodeValidacionSerial = validacionSerial ? base64.encode(validacionSerial) : "";
  console.log("encodeValidacionSerial",encodeValidacionSerial,validacionSerial);

  const encodeValidarCodigo = validarCodigo ? base64.encode(validarCodigo) : "";
  console.log("encodeValidarCodigo",encodeValidarCodigo,validarCodigo);

  const encodeValidarCodigoQR = validarCodigoQR ? base64.encode(validarCodigoQR) : "";
  console.log("encodeValidarCodigoQR",encodeValidarCodigoQR,validarCodigoQR)

const limpiarData = () => {
  console.log("limpiar")
  setNuevaMatriz([]);
  setNoSorteo("");
  setValidacionSerial("");
  setValidarCodigo("");
  setValidarCodigoQR("");
  setIsValid(false);
  setIsValid2(false);
  setIsValid3(false);
  setIsValid4(false);
  setIsValid5(false);
  setActive(0);
  setState({
    ...state,
    textRecognition: null,
    textRecognitionMatriz: null,
    textRecognitionSerial: null,
    qr: '',
    scan: false,
    ScanResult: false,
    result: null,
    //ESCANEAR QR
    qrSerial: '',
    scanSerial: false,
    ScanResultSerial: false,
    resultSerial: null,

    errors: false,
    errors2: false,
    errors3: false,
    errors4: false,
    errors5: false,
  });
}

const dataPrueba = {
  instantMatrix: "123456789",
  prizeAmount: 5000,
   message: {
       code: "000",
           description: "Respuesta Exitosa."
   }
} 

  const url = 'http://200.44.165.134:8041/triplegordoweb.Servicio.juego/ServicioTGWebJuego.svc/ServicioTGWebJuego/';
  const UriTemplate = 'ValidarInstantaneo';

  const apiValidarTicket = async () => {
    setResponseLoading(true);
     return fetch(url + UriTemplate, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        instantMatrix: /* "NDM=" */ encodeSinEspacio, //Enviar Dato Encriptado (Matriz Instantaneo),
	      lotteryNumber : /* "MDAy" */encodeNoSorteo, //Enviar Dato Encriptado (Número de Sorteo), MDIK
        openTriple: /* "OTQ0" */encodeValidacionSerial, //Enviar Dato Encriptado (Triple Abierto),
	      operatorCode : /* "NzM0MDE0MDU=" */encodeValidarCodigoQR, //Enviar Dato Encriptado (Código de Operador),
	      ticketSerial: /* "MDAyMDAwMDQ5NTky" */encodeValidarCodigo //Enviar Dato Encriptado (Serial del Ticket)
      }),
    })
      .then(response => response.json())
      .then(function (response) {
        setResponseLoading(false);
        console.log("response",response)
        if (response.message.code === "000" /* && response.message.code === "048" */) {
     
          setDataResult(response);
          // setDataResult(dataPrueba);
          console.log("data",dataResult)
          abrirModal();
          setLoading(false);
          Alert.alert(
            '¡Felicidades!','Eres ganador de un premio de' + ' ' + '$' + response.prizeAmount,
            [
              {
                text: 'Aceptar',
                onPress: () => console.log("gano"),
              },
            ],
          );
        }else if(response.message.code === "998"){
          Alert.alert(
            'Triple Gordo Informa',
            response.message.description,
            [
              {
                text: 'Aceptar',
                onPress: () => console.log("ok"),
              },
            ],
          );
        }
        else{
          Alert.alert(
            'Triple Gordo Informa',
            response.message.description,
            [
              {
                text: 'Aceptar',
                onPress: () => salirResultado(),
              },
            ],
          );
        }
      })
      .catch(error => {
        setResponseLoading(false);
        console.error("error",error);
      });
     
  };
  
  const iconScanColor = '#00000000';
  const [state, setState] = useState({
    loading: false,
    image: null,
    textRecognition: null,
    textRecognitionMatriz: null,
    textRecognitionSerial: null,
    toast: {
      message: '',
      isVisible: false,
    },
    //ESCANEAR CODIGO
    qr: '',
    scan: false,
    ScanResult: false,
    result: null,
    //ESCANEAR QR
    qrSerial: '',
    scanSerial: false,
    ScanResultSerial: false,
    resultSerial: null,
    // ABRIR MODAL
    modalOpen: false,
    // steper
    errors: false,
    errors2: false,
    errors3: false,
    errors4: false,
    errors5: false,
  });
  const image = require('../assets/fondo_recortado.png');
  const [visibility, setVisibility] = useState(false);
  const [visibilityResult, setVisibilityResult] = useState(false);
  const [visibilitySerial, setVisibilitySerial] = useState(false);
  const [isValid, setIsValid] = useState(false);
  const [isValid2, setIsValid2] = useState(false);
  const [isValid3, setIsValid3] = useState(false);
  const [isValid4, setIsValid4] = useState(false);
  const [isValid5, setIsValid5] = useState(false);

  // FUNCIONES DEL STEPER (PASO A PASO)
  const onNextStep = () => {
    if (!isValid){
      setState({
        ...state,
        errors: true,
      })
    } else {
      setState({
        ...state,
        errors: false,
      })
    }
  };

  const onNextStep2 = () => {
    if (!isValid2){
      setState({
        ...state,
        errors2: true,
      })
    } else {
      setState({
        ...state,
        errors2: false,
      })
      setActive(1)
    }
  };

  const onNextStep3 = () => {
    if (!isValid3){
      setState({
        ...state,
        errors3: true,
      })
    } else {
      setState({
        ...state,
        errors3: false,
      })
      setActive(2)
    }
  };

  const onNextStep4 = () => {
    if (!isValid4){
      setState({
        ...state,
        errors4: true,
      })
    } else {
      setState({
        ...state,
        errors4: false,
      })
      setActive(3)
    }
  };

  const onSubmitSteps = () => {
    if (!isValid5){
      setState({
        ...state,
        errors5: true,
      })
console.log("falla")
    } else {
      setState({
        ...state,
        errors5: false,
      }),
      setActive(4)
      mensajeFinal();
    }
  };

  const mensajeFinal = () => {
    Alert.alert(
      '¡Sus datos serán verificados!',
      '¿Desea continuar?',
      [
        {
          text: 'Aceptar',
          onPress: () => apiValidarTicket(),
        },
        {
          text: 'Cancelar',
          onPress: () => console.log('bien'),
        },
      ],
    );
  };

  // ESCANEAR CODIGO BARRA
  const onSuccess = e => {
    // console.log('e BARRA', e);
    const valor = {qr: e.data};
    console.log('BARRA valor', valor);
    const isValor = valor.qr && valor.qr.length === 12;

    setState({
      ...state,
      result: e,
      scan: false,
      ScanResult: true,
      ...(isValor ? {qr: valor.qr} : {}),
    });
    setIsValid2(true);

    if (!isValor) mensajeError();
    else salirEscaner();
  };

  const activeQR = () => {
    setState({
      ...state,
      scan: true,
    });
    setModalVisibility(true);
  };

  const scanAgain = () => {
    setState({
      ...state,
      scan: true,
      ScanResult: false,
      qr: '',
    });
    setModalVisibility(true);
  };

  const setModalVisibility = visible => {
    setVisibility(visible);
  };

  const mensajeError = () => {
    Alert.alert(
      'Triple Gordo informa',
      'No se pudo leer correctamente el código QR por favor intente nuevamente',
      [
        {
          text: 'OK',
          onPress: () => setModalVisibility(!visibility),
        },
      ],
    );
  };

  const salirEscaner = () => {
    // setState({...state, scan: false});
    setModalVisibility(!visibility);
  };

  const makeSlideOutTranslation = (translationType, fromValue) => {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18,
      },
      to: {
        [translationType]: fromValue,
      },
    };
  };

  // ESCANEAR CODIGO QR
  const onSuccessQR = e => {
    console.log("e Serial",e);
    const valorSerial = {qrSerial: e.data};
    console.log("ada", valorSerial)
    const isValorSerial = valorSerial.qrSerial && valorSerial.qrSerial.length === 8;

    setState({
      ...state,
      resultSerial: e,
      scanSerial: false,
      ScanResultSerial: true,
      ...(isValorSerial ? {qrSerial: valorSerial.qrSerial} : {}),
    });
    setIsValid3(true);
    if (!isValorSerial) mensajeErrorSerial();
    else salirEscanerSerial();
  };

  const activeQRSerial = () => {
    setState({
      ...state,
      scanSerial: true,
    });
    setModalVisibilitySerial(true);
  };

  const scanAgainSerial = () => {
    setState({
      ...state,
      scanSerial: true,
      ScanResultSerial: false,
      // qrSerial: '',
    });
    setModalVisibilitySerial(true);
  };

  const setModalVisibilitySerial = visible => {
    setVisibilitySerial(visible);
  };

  const mensajeErrorSerial = () => {
    Alert.alert(
      'Triple Gordo informa',
      'No se pudo leer correctamente el código QR por favor intente nuevamente',
      [
        {
          text: 'OK',
          onPress: () => setModalVisibilitySerial(!visibilitySerial),
        },
      ],
    );
  };

  const salirEscanerSerial = () => {
    // setState({...state, scanSerial: false});
    setModalVisibilitySerial(!visibilitySerial);
  };

    // abrir modal resultado
   const abrirModal = () => {
    setState({
      ...state,
      modalOpen: true,
    });
    setModalVisibilityResult(true);
  };

  const setModalVisibilityResult = visible => {
    setVisibilityResult(visible);
  };

  const salirResultado = () => {
    limpiarData();
    setModalVisibilityResult(!visibilityResult);
    navigation.navigate("Escanear");
  };


  //Escanear imagen numero serial
  const onPressNoSerial = type => {
    setState({...state, loading: true});
    setIsValid(true);
    type === 'capture'
      ? launchCamera({mediaType: 'image'}, onImageSelect)
      : launchImageLibrary({mediaType: 'image'}, onImageSelect);
  };

  const onImageSelect = async media => {
    if (!media) {
      setState({...state, loading: false});
      return;
    }
    if (!!media && media.assets) {
      const file = media.assets[0].uri;
      
      const textRecognition = await RNTextDetector.detectFromUri(file);
      const INFLIGHT_IT = 'Inflight IT';
      //if match toast will appear
      const matchText = textRecognition.findIndex(item =>
        item.text.match(INFLIGHT_IT),
      );
      {
        console.log('item alfredo', matchText);
      }
      setState({
        ...state,
        textRecognition,
        image: file,
      });
      setVisibility(false);
    }
  };

  //Escanear imagen MATRIZ
  const onPressMatriz = type => {
    setState({...state, loading: true});
    setIsValid4(true);
    type === 'capture'
      ? launchCamera({mediaType: 'image'}, onImageSelectMatriz)
      : launchImageLibrary({mediaType: 'image'}, onImageSelectMatriz);
  };

  const onImageSelectMatriz = async media => {
    if (!media) {
      setState({...state, loading: false});
      return;
    }
    if (!!media && media.assets) {
      const file = media.assets[0].uri;
      const textRecognitionMatriz = await RNTextDetector.detectFromUri(file);
      const INFLIGHT_IT = 'Inflight IT';
      //if match toast will appear
      const matchTextMatriz = textRecognitionMatriz.findIndex(item =>
        item.text.match(INFLIGHT_IT),
      );
      {
        console.log('itemMatriz', matchTextMatriz);
      }
      setState({
        ...state,
        textRecognitionMatriz,
        image: file,
        toast: {
          message: matchTextMatriz > -1 ? 'Ohhh i love this company!!' : '',
          isVisible: matchTextMatriz > -1,
        },
        loading: false,
      });
    }
  };

  //Escanear imagen SERIAL ABIERTO
  const onPressSerial = type => {
    setState({...state, loading: true});
    setIsValid5(true);
    type === 'capture'
      ? launchCamera({mediaType: 'image'}, onImageSelectSerial)
      : launchImageLibrary({mediaType: 'image'}, onImageSelectSerial);
  };

  const onImageSelectSerial = async media => {
    if (!media) {
      setState({...state, loading: false});
      return;
    }
    if (!!media && media.assets) {
      const file = media.assets[0].uri;
      const textRecognitionSerial = await RNTextDetector.detectFromUri(file);
      const INFLIGHT_IT = 'Inflight IT';
      //if match toast will appear
      const matchTextSerial = textRecognitionSerial.findIndex(item =>
        item.text.match(INFLIGHT_IT),
      );
      {
        console.log('itemMatriz', matchTextSerial);
      }
      setState({
        ...state,
        textRecognitionSerial,
        image: file,
        toast: {
          message: matchTextSerial > -1 ? 'Ohhh i love this company!!' : '',
          isVisible: matchTextSerial > -1,
        },
        loading: false,
      });
    }
  };

// COMPONENTES PARA VERIFICAR
  const NumeroSorteo = () => {
    const nSorteo = require('../assets/nSorteo.jpg');
    const validacionNoSorteo = state.textRecognition && state.textRecognition.length > 0 ? state.textRecognition[0].text.slice(7, 9) === 'N°' : "";
    setNoSorteo(state.textRecognition && state.textRecognition.length > 0 ? state.textRecognition[0].text.slice(10, 13) : "");
    return (
      <View style={styles.contentComponent}>
        <View>
          <Text style={styles.textPasos}>
            Paso #1: Escanear Número de Sorteo
          </Text>
          <View>
            {state.textRecognition && validacionNoSorteo ? (
              <Text style={styles.infoPasos}>N° {sliceNoSorteo}</Text>
            ) : (
              <>
                <Text style={styles.mensajeError}>
                  {!state.textRecognition && !validacionNoSorteo ? (
                    <></>
                  ) : (
                    `¡Debe escanear el número del sorteo! ¡Intente nuevamente!`
                  )}
                </Text>
              </>
            )}
          </View>
          <View style={{marginBottom: 15}}>
            <Image
              source={nSorteo}
              style={{
                flex: 1,
                alignSelf: 'center',
                justifyContent: 'center',
                width: '100%',
                height: 185,
              }}
            />
          </View>
        </View>

        <View style={styles.divBtnPasos}>
          <TouchableOpacity
            style={styles.buttonPasos}
            onPress={() => onPressNoSerial('capture')}>
            <Text style={styles.buttonTextStyle}>TOMAR FOTO</Text>
          </TouchableOpacity>
        </View>
        {state.toast.isVisible &&
          ToastAndroid.showWithGravityAndOffset(
            state.toast.message,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          )}
      </View>
    );
  };

  const CodigoBarra = () => {
    const codigoBarra = require('../assets/codigoBarra.jpg');
    setValidarCodigo(state.qr);
    console.log("validar",validarCodigo)

    return (
      <View style={styles.contentComponent}>
        <View>
          <Text style={styles.textPasos}>
            Paso #2: Escanear Código de Barra
          </Text>
          <View>
            {state.qr ? (
              <Text style={styles.infoPasos}>{validarCodigo}</Text>
            ) : (
              <>
              <Text style={styles.mensajeError}>
                {!state.qr ? <></> : `¡Debe escanear el código de barra! ¡Intente nuevamente!`}
              </Text>
              
              </>
            )}
          </View>
          <View style={{marginBottom: 15}}>
            <Image
              source={codigoBarra}
              style={{
                flex: 1,
                alignSelf: 'center',
                justifyContent: 'center',
                width: '100%',
                height: 185,
              }}
            />
          </View>
        </View>

        {/* BOTÓN VALIDAR */}
        {!state.scan && !state.ScanResult && (
          <View style={styles.alignBoton}>
            <TouchableOpacity
              onPress={() => activeQR()}
              style={styles.buttonPasos}>
              <Text style={styles.buttonTextStyle}>ESCANEAR CÓDIGO</Text>
            </TouchableOpacity>
          </View>
        )}
        {state.ScanResult && (
          <Fragment>
            <View style={styles.alignBoton}>
              <TouchableOpacity
                onPress={() => scanAgain()}
                style={styles.buttonPasos}>
                <Text style={styles.buttonTextStyle}>ESCANEAR DE NUEVO!</Text>
              </TouchableOpacity>
            </View>
          </Fragment>
        )}
      </View>
    );
  };

  const CodigoQR = () => {
   const codigoQr = require('../assets/serialTicket.jpg');
   setValidarCodigoQR(state.qrSerial);

    return (
      <View style={styles.contentComponent}>
        <View>
          <Text style={styles.textPasos}>Paso #3: Escanear Código QR</Text>
          <View>
            {state.qrSerial ? (
              <Text style={styles.infoPasos}>{validarCodigoQR}</Text>
              ) : (
                <>
                <Text style={styles.mensajeError}>
                  {!validarCodigoQR ? <></> : `¡Debe escanear el número código QR! ¡Intente nuevamente!`}
                </Text>
                
                </>
              )}
          </View>
          <View style={{marginBottom: 15}}>
            <Image
              source={codigoQr}
              style={{
                flex: 1,
                alignSelf: 'center',
                justifyContent: 'center',
                width: '100%',
                height: 185,
              }}
            />
          </View>
        </View>

        {/* BOTÓN ESCANEAR CODIGO QR */}
        {!state.scanSerial && !state.ScanResultSerial && (
          <View style={styles.alignBoton}>
            <TouchableOpacity
              onPress={() => activeQRSerial()}
              style={styles.buttonPasos}>
              <Text style={styles.buttonTextStyle}>ESCANEAR CÓDIGO QR</Text>
            </TouchableOpacity>
          </View>
        )}
        {state.ScanResultSerial && (
          <Fragment>
            <View style={styles.alignBoton}>
              <TouchableOpacity
                onPress={() => scanAgainSerial()}
                style={styles.buttonPasos}>
                <Text style={styles.buttonTextStyle}>ESCANEAR DE NUEVO!</Text>
              </TouchableOpacity>
            </View>
          </Fragment>
        )}
      </View>
    );
  };

  const EscanearMatriz = () => {

    const imgMatriz = require('../assets/matriz.jpg');
    const validacionMatriz = (state.textRecognitionMatriz && state.textRecognitionMatriz.length > 0) ? (state.textRecognitionMatriz[0].text) : "";

    setSinEspacio(validacionMatriz.replace(/\s+/g, ''));
    console.log("sinEspacio",sinEspacio);
    // const nuevaMatriz = [];
    setNuevaMatriz(validacionMatriz);
    // nuevaMatriz.push(validacionMatriz);
    console.log("nuevaMatriz",nuevaMatriz)   
    const expresion = /^[0-9]*$/;
    const resultado = expresion.test(sinEspacio);

    return (
      <View style={styles.contentComponent}>
        <View>
          <Text style={styles.textPasos}>Paso #4: Escanear Matriz</Text>
          <View>
            
            {state.textRecognitionMatriz && state.textRecognitionMatriz.length > 0 && resultado ? 
            (
              <Text style={styles.infoPasos} >
                  {nuevaMatriz}
                </Text>
            ) : (
              <>
              <Text style={styles.mensajeError}>
                {!state.textRecognitionMatriz && !nuevaMatriz ? <></> : `¡Debe escanear la matriz! ¡Intente nuevamente!`}
              </Text>
              
              </>
            )
              }
          </View>
          <View style={{marginBottom: 15}}>
            <Image
              source={imgMatriz}
              style={{
                flex: 1,
                alignSelf: 'center',
                justifyContent: 'center',
                width: '100%',
                height: 185,
              }}
            />
          </View>
        </View>

        <View style={styles.divBtnPasos}>
          <TouchableOpacity
            style={styles.buttonPasos}
            onPress={() => onPressMatriz('capture')}>
            <Text style={styles.buttonTextStyle}>TOMAR FOTO MATRIZ</Text>
          </TouchableOpacity>
        </View>
        {state.toast.isVisible &&
          ToastAndroid.showWithGravityAndOffset(
            state.toast.message,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          )}
      </View>
    );
  };

  const SerialAbierto = () => {

    const serialAbierto = require('../assets/serialAbierto.jpg');
    setValidacionSerial((state.textRecognitionSerial && state.textRecognitionSerial.length > 0) ? (state.textRecognitionSerial[0].text) : "");

    const expresionSerial = /^[0-9]*$/;
    const resultadoSerial = expresionSerial.test(validacionSerial);

    return (
      <View style={styles.contentComponent}>
        <View>
          <Text style={styles.textPasos}>Paso #5: Escanear Serial Abierto</Text>
          <View>
            {state.textRecognitionSerial && state.textRecognitionSerial.length > 0 && resultadoSerial ? (
              <Text style={styles.infoPasos}>{validacionSerial}</Text>
            ) : (
              <>
                <Text style={styles.mensajeError}>
                  {!state.textRecognitionSerial && !validacionSerial ? (
                    <></>
                  ) : (
                    `¡Debe escanear el serial abierto! ¡Intente nuevamente!`
                  )}
                </Text>
              </>
            )}
          </View>
          <View style={{marginBottom: 15}}>
            <Image
              source={serialAbierto}
              style={{
                flex: 1,
                alignSelf: 'center',
                justifyContent: 'center',
                width: '100%',
                height: 185,
              }}
            />
          </View>
        </View>
        <View style={styles.divBtnPasos}>
          <TouchableOpacity
            style={styles.buttonPasos}
            onPress={() => {responseLoading ? ()=>{} : onPressSerial('capture')}}>
            <Text style={styles.buttonTextStyle}>TOMAR FOTO SERIAL</Text>
          </TouchableOpacity>
        </View>
        {state.toast.isVisible &&
          ToastAndroid.showWithGravityAndOffset(
            state.toast.message,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          )}
      </View>
    );
  };

 const [arrayCuadriculaMatriz,setArrayCuadriculaMatriz] = useState([]);

  useEffect(() => {
    console.log("chemar",dataResult)
    if(dataResult.instantMatrix){
      // console.log("matriz up",data.instantMatrix)
      // CUADRICULA
      const cuadriculaMatriz = [];
      cuadriculaMatriz.push(dataResult.instantMatrix);
      setArrayCuadriculaMatriz([...cuadriculaMatriz[0]]);
      console.log("array",arrayCuadriculaMatriz)
    } 
  }, [dataResult])
  

  const progressStepsStyle = {
    activeStepIconBorderColor: '#0a0d64',
    activeLabelColor: '#0a0d64',
    activeStepNumColor: 'white',
    activeStepIconColor: '#0a0d64',
    completedStepIconColor: '#0a0d64',
    completedProgressBarColor: '#0a0d64',
    completedCheckColor: 'white'
  };

const DeviceWidth = Dimensions.get('window').width;

  return (
    <>
      {/* MODAL CODIGO BARRA */}
      <View>
        <Modal animationType={'slide'} transparent={false} visible={visibility}>
          <View style={styles.cardView}>
            <View>
              {/* SCANER */}
              {state.scan && (
                <QRCodeScanner
                  reactivate={true}
                  onRead={e => onSuccess(e)}
                  showMarker={true}
                  ref={node => {
                    state.scanner = node;
                  }}
                  cameraStyle={{height: SCREEN_HEIGHT}}
                  customMarker={
                    <View style={styles.rectangleContainer}>
                      <View style={styles.topOverlay}>
                        <Text
                          style={{
                            fontSize: 30,
                            color: 'white',
                            fontWeight: 'bold',
                          }}>
                          Escanear Código de Barra
                        </Text>
                      </View>

                      <View style={{flexDirection: 'row'}}>
                        <View style={styles.leftAndRightOverlay} />
                        <View style={styles.rectangle}>
                          <Icon
                            name="ios-qr-scanner"
                            size={SCREEN_WIDTH * 0.73}
                            color={iconScanColor}
                          />
                          <Animatable.View
                            style={styles.scanBar}
                            direction="alternate-reverse"
                            iterationCount="infinite"
                            duration={1700}
                            easing="linear"
                            animation={makeSlideOutTranslation(
                              'translateY',
                              SCREEN_WIDTH * -0.54,
                            )}
                          />
                        </View>

                        <View style={styles.leftAndRightOverlay} />
                      </View>

                      <View style={styles.bottomOverlay} />
                    </View>
                  }
                />
              )}
              {/* <View style={styles.alignBoton}>
                <TouchableOpacity
                  onPress={() => salirEscaner()}
                  style={styles.btnSalir}>
                  <Text style={styles.buttonTextStyle}>Salir</Text>
                </TouchableOpacity>
              </View> */}
            </View>
          </View>
        </Modal>
      </View>

      {/* MODAL CODIGO QR */}
      <View>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={visibilitySerial}>
          <View style={styles.cardView}>
            <View>
              {/* SCANER */}
              {state.scanSerial && (
                <QRCodeScanner
                  reactivate={true}
                  onRead={e => onSuccessQR(e)}
                  showMarker={true}
                  ref={node => {
                    state.scannerSerial = node;
                  }}
                  cameraStyle={{height: SCREEN_HEIGHT}}
                  customMarker={
                    <View style={styles.rectangleContainer}>
                      <View style={styles.topOverlay}>
                        <Text
                          style={{
                            fontSize: 30,
                            color: 'white',
                            fontWeight: 'bold',
                          }}>
                          Escanear Código QR
                        </Text>
                      </View>

                      <View style={{flexDirection: 'row'}}>
                        <View style={styles.leftAndRightOverlay} />
                        <View style={styles.rectangle}>
                          <Icon
                            name="ios-qr-scanner"
                            size={SCREEN_WIDTH * 0.73}
                            color={iconScanColor}
                          />
                          <Animatable.View
                            style={styles.scanBar}
                            direction="alternate-reverse"
                            iterationCount="infinite"
                            duration={1700}
                            easing="linear"
                            animation={makeSlideOutTranslation(
                              'translateY',
                              SCREEN_WIDTH * -0.54,
                            )}
                          />
                        </View>

                        <View style={styles.leftAndRightOverlay} />
                      </View>

                      <View style={styles.bottomOverlay} />
                    </View>
                  }
                />
              )}
              {/* <View style={styles.alignBoton}>
                <TouchableOpacity
                  onPress={() => salirEscanerSerial()}
                  style={styles.btnSalir}>
                  <Text style={styles.buttonTextStyle}>Salir</Text>
                </TouchableOpacity>
              </View> */}
            </View>
          </View>
        </Modal>
      </View>

      {/* MODAL RESULTADO */}
      <View>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={visibilityResult}>
          <View style={styles.cardView}>
            <View>
              {state.modalOpen && (
                <View style={{flex: 1, backgroundColor: '#fff'}}>
                  <Header />
                  <SocialNetworks />
                  {/* TEXTO */}
                  <TituloText title="RESULTADO DE LA VERIFICACIÓN" />
                  {isLoading ? (
                    <ActivityIndicator />
                  ) : (
                    <>
                      <View style={{paddingLeft: 20}}>
                        {/* CUADRICULA */}
                        <View
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: Platform.OS === 'android' ? 20 : 0,
                          }}>
                          <View style={styles.borderCuariculas}>
                            {/* INICIO COLUMNA 0 */}
                            <View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text
                                  style={
                                    Platform.OS === 'android'
                                      ? styles.esitloMontos2
                                      : styles.esitloMontos3
                                  }>
                                  6.000
                                </Text>
                                <Text
                                  style={
                                    Platform.OS === 'android'
                                      ? styles.esitloMontos3
                                      : styles.esitloMontos4
                                  }>
                                  DÓLARES
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/6000.png')}
                                />
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.esitloMontos3}>
                                  {sliceNoSorteo < '003' ? '$ 25' : '$ 10'}
                                </Text>
                                <Text style={styles.esitloMontos4}>
                                  DÓLARES
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/flechaDerecha.png')}
                                />
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.esitloMontos3}>
                                  {sliceNoSorteo < '003' ? '$ 100' : '$50'}
                                </Text>
                                <Text style={styles.esitloMontos4}>
                                  DÓLARES
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/flechaDerecha.png')}
                                />
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.esitloMontos3}>
                                  {sliceNoSorteo < '003' ? '$ 2500' : '$ 15'}
                                </Text>
                                <Text style={styles.esitloMontos4}>
                                  DÓLARES
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/flechaDerecha.png')}
                                />
                              </View>
                              {sliceNoSorteo < '003' ? (
                                <></>
                              ) : (
                                <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text
                                  style={
                                    Platform.OS === 'android'
                                      ? styles.esitloMontos3
                                      : styles.esitloMontos4
                                  }>
                                  Reintegro
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/newArrow.png')}
                                />
                              </View>
                              )}
                            </View>
                            {/* INICIO COLUMNA 1 */}
                            <View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.esitloMontos3}>
                                  {sliceNoSorteo < '003' ? '$ 10' : '$ 25'}
                                </Text>
                                <Text style={styles.esitloMontos4}>
                                  DÓLARES
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/flechaAbajo.png')}
                                />
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderColor: '#0a0d64',
                                  borderTopWidth: 4,
                                  borderLeftWidth: 4,
                                  borderRightWidth: 1,
                                  borderBottomWidth: 1,
                                  borderTopLeftRadius: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[0]}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderColor: '#0a0d64',
                                  borderLeftWidth: 4,
                                  borderTopWidth: 1,
                                  borderRightWidth: 1,
                                  borderBottomWidth: 1,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[3]}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderColor: '#0a0d64',
                                  borderBottomWidth: 4,
                                  borderLeftWidth: 4,
                                  borderTopWidth: 1,
                                  borderRightWidth: 1,
                                  borderBottomLeftRadius: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[6]}
                                </Text>
                              </View>
                              {sliceNoSorteo < '003' ? (
                                <></>
                              ) : (
                                <View
                                  style={{
                                    width: DeviceWidth * 0.19,
                                    height: DeviceWidth * 0.15,
                                    backgroundColor: 'transparent',
                                  }}></View>
                              )}                          
                            </View>
                            {/* INICIO COLUMNA 2 */}
                            <View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.esitloMontos3}>
                                {sliceNoSorteo < '003' ? '$ 50' : '$ 100'}
                                </Text>
                                <Text style={styles.esitloMontos4}>
                                  DÓLARES
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/flechaAbajo.png')}
                                />
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderColor: '#0a0d64',
                                  borderTopWidth: 4,
                                  borderBottomWidth: 1,
                                  borderLeftWidth: 1,
                                  borderRightWidth: 1,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[1]}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderTopWidth: 1,
                                  borderBottomWidth: 1,
                                  borderLeftWidth: 1,
                                  borderRightWidth: 1,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[4]}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderColor: '#0a0d64',
                                  borderBottomWidth: 4,
                                  borderTopWidth: 1,
                                  borderLeftWidth: 1,
                                  borderRightWidth: 1,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[7]}
                                </Text>
                              </View>
                              {sliceNoSorteo < '003' ? (
                                <></>
                              ) : (
                                <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                }}></View>
                              )}
                            </View>
                            {/* INICIO COLUMNA 3 */}
                            <View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.esitloMontos3}>
                                  {sliceNoSorteo < '003' ? '$ 15' : '$ 2.500'}
                                </Text>
                                <Text style={styles.esitloMontos4}>
                                  DÓLARES
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/flechaAbajo.png')}
                                />
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderColor: '#0a0d64',
                                  borderTopWidth: 4,
                                  borderRightWidth: 4,
                                  borderBottomWidth: 1,
                                  borderLeftWidth: 1,
                                  borderTopRightRadius: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[2]}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderColor: '#0a0d64',
                                  borderTopWidth: 1,
                                  borderRightWidth: 4,
                                  borderBottomWidth: 1,
                                  borderLeftWidth: 1,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[5]}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                  borderColor: '#0a0d64',
                                  borderBottomWidth: 4,
                                  borderRightWidth: 4,
                                  borderTopWidth: 1,
                                  borderLeftWidth: 1,
                                  borderBottomRightRadius: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text style={styles.estiloNumeros}>
                                  {arrayCuadriculaMatriz[8]}
                                </Text>
                              </View>
                              {sliceNoSorteo < '003' ? (
                                <></>
                              ) : (
                                <View
                                style={{
                                  width: DeviceWidth * 0.19,
                                  height: DeviceWidth * 0.15,
                                  backgroundColor: 'transparent',
                                }}></View>
                              )}
                            </View>
                            {/* INICIO COLUMNA 4 */}
                            <View>
                             {sliceNoSorteo < '003' ? (
                                <>
                                <View
                                style={{
                                  width: DeviceWidth * 0.2,
                                  height: DeviceWidth * 0.19,
                                  backgroundColor: 'transparent',
                                  alignItems: 'center',
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}>
                                <Text
                                  style={
                                    Platform.OS === 'android'
                                      ? styles.esitloMontos3
                                      : styles.esitloMontos4
                                  }>
                                  Reintegro
                                </Text>
                                <Image
                                  style={styles.tinyLogoMontos}
                                  source={require('../assets/flechaIzquierda.png')}
                                />
                              </View>
                              <View
                              style={{
                                width: DeviceWidth * 0.2,
                                height: DeviceWidth * 0.19,
                                backgroundColor: 'transparent',
                              }}></View>
                            <View
                              style={{
                                width: DeviceWidth * 0.2,
                                height: DeviceWidth * 0.19,
                                backgroundColor: 'transparent',
                              }}></View>
                            <View
                              style={{
                                width: DeviceWidth * 0.2,
                                height: DeviceWidth * 0.19,
                                backgroundColor: 'transparent',
                              }}></View>
                                </>
                             ) : (
                              <>
                              <View
                              style={{
                                width: DeviceWidth * 0.2,
                                height: DeviceWidth * 0.19,
                                backgroundColor: 'transparent',
                              }}></View>
                            <View
                              style={{
                                width: DeviceWidth * 0.2,
                                height: DeviceWidth * 0.19,
                                backgroundColor: 'transparent',
                              }}></View>
                            <View
                              style={{
                                width: DeviceWidth * 0.2,
                                height: DeviceWidth * 0.19,
                                backgroundColor: 'transparent',
                              }}></View>
                            <View
                              style={{
                                width: DeviceWidth * 0.2,
                                height: DeviceWidth * 0.19,
                                backgroundColor: 'transparent',
                              }}></View>
                            <View
                              style={{
                                width: DeviceWidth * 0.2,
                                height: DeviceWidth * 0.19,
                                backgroundColor: 'transparent',
                              }}></View>
                              </>
                             )}
                              
                            </View>
                          </View>
                        </View>
                      </View>
                    </>
                  )}
                </View>
              )}
              <View style={styles.alignBoton}>
                <TouchableOpacity
                  onPress={() => salirResultado()}
                  style={styles.btnSalir}>
                  <Text style={styles.buttonTextStyle}>Salir</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>

      {/* HEADER */}
      <Header />
      {/* TEXTO */}
      <TituloText title="VERIFICACIÓN DE TICKET" />

      <View style={{flex: 1}}>
        {responseLoading ? (
          <View style={styles.loadingCenter}>
            <ActivityIndicator size="large" color="#0a0d64" />
          </View>
        ) : (
          <></>
        )}
        <Image
          source={image}
          resizeMode="cover"
          style={{
            position: 'absolute',
            left: 0,
            width: '100%',
            height: '100%',
            top: 0,
          }}
        />

        <ScrollView
          style={styles.scrollViewStyle}
          contentContainerStyle={{flexGrow: 1, padding: 15}}>
          <View style={{flex: 1}}>
            <ProgressSteps {...progressStepsStyle} activeStep={active}>
              <ProgressStep
                onNext={() => onNextStep()}
                errors={state.errors}
                nextBtnText={'Siguiente'}
                nextBtnStyle={styles.stepperColor}
                nextBtnTextStyle={styles.stepperTextColor}>
                <NumeroSorteo />
              </ProgressStep>
              <ProgressStep
                onNext={() => onNextStep2()}
                errors={state.errors2}
                previousBtnText={'Atrás'}
                nextBtnText={'Siguiente'}
                nextBtnStyle={styles.stepperColor}
                nextBtnTextStyle={styles.stepperTextColor}
                previousBtnStyle={styles.stepperColor}
                previousBtnTextStyle={styles.stepperTextColor}>
                <CodigoBarra />
              </ProgressStep>
              <ProgressStep
                onNext={() => onNextStep3()}
                errors={state.errors3}
                previousBtnText={'Atrás'}
                nextBtnText={'Siguiente'}
                nextBtnStyle={styles.stepperColor}
                nextBtnTextStyle={styles.stepperTextColor}
                previousBtnStyle={styles.stepperColor}
                previousBtnTextStyle={styles.stepperTextColor}>
                <CodigoQR />
              </ProgressStep>
              <ProgressStep
                onNext={() => onNextStep4()}
                errors={state.errors4}
                previousBtnText={'Atrás'}
                nextBtnText={'Siguiente'}
                nextBtnStyle={styles.stepperColor}
                nextBtnTextStyle={styles.stepperTextColor}
                previousBtnStyle={styles.stepperColor}
                previousBtnTextStyle={styles.stepperTextColor}>
                <EscanearMatriz />
              </ProgressStep>
              <ProgressStep
                nextBtnDisabled={responseLoading}
                previousBtnDisabled={responseLoading}
                onSubmit={() => {
                  responseLoading ? () => {} : onSubmitSteps();
                }}
                errors={state.errors5}
                previousBtnText={'Atrás'}
                finishBtnText={'Verificar'}
                nextBtnStyle={styles.stepperColor}
                nextBtnTextStyle={styles.stepperTextColor}
                previousBtnStyle={styles.stepperColor}
                previousBtnTextStyle={styles.stepperTextColor}>
                <SerialAbierto />
              </ProgressStep>
            </ProgressSteps>
          </View>
        </ScrollView>
      </View>
      <SocialNetworks />
    </>
  );
};

export default VerificarTicket;
