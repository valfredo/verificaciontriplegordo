import React, {Fragment, useState} from 'react';
import Header from "../components/Header/Header"
import Footer from "../components/Footer/Footer"
import {
  Text,
  View,
  Dimensions,
  Alert,
  TouchableOpacity,
  ScrollView,
  Modal,
  Image,
  Linking,
  Platform,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from '../assets/scanStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';
import TituloText from '../components/TituloText/TituloText';
import SocialNetworks from '../components/SocialNetworks/SocialNetworks';
import Header from '../components/Header/Header';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

let pjson = require('../../package.json');
const version = pjson.version;
console.log("pjson",version)

const EscanearCodigo = ({navigation}) => {
  const url = "http://200.44.165.134:8041/triplegordoweb.Servicio.general/ServicioTGWebGeneral.svc/ServicioTGWebGeneral/IniciarSesionApp";

  const versionApp = () => {
    console.log("Hola logueo")
    fetch(url, {
      method: 'POST',
      body: JSON.stringify({
        login: "jogrecolpe",
        password: "abc.123",
        url: "triplegordo.com.ve",
        version: "1.1" /* version */
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
    
      .then(response => response.json())
      .then(function (response) {
        console.log("respon",response)
        if (response.message.code === "000"){
           console.log("ok")
        }
        else if (response.message.code === "050") {
          Alert.alert(
            '¡Triple Gordo informa!',
            response.message.description,
            [
              {
                text: 'Actualizar',
                onPress: () => {
                  Linking.openURL(
                    'https://play.google.com/store/apps/details?id=com.verificacion_triple_gordo',
                  );
                },
              },
            ],
          );
        }
      })
      .catch((error) => {
        console.error("error",error);
      });
  };

  useFocusEffect(
    React.useCallback(() => {
      versionApp();
    }, [])
  );

  const [state, setState] = useState({
    qr: '',
    scan: false,
    ScanResult: false,
    result: null,
  });
  const [visibility, setVisibility] = useState(false);
  // console.log("nSorteo 1",nSorteo)

  const onSucces = e => {
    const valor = {qr: e.data};
    const datosSorteo = valor.qr;
      // console.log("datosSorteo",datosSorteo)

    console.log('que tien valor',valor)
    console.log('que tien datosSorteo',datosSorteo )
    console.log('que tien valor . lengh',valor.qr.length )

    // VALIDAR GANADOR
    const cuadriculaValidar = [];
    cuadriculaValidar.push(datosSorteo.slice(-12, -3));
    const arrayCuadriculaValidar = [...cuadriculaValidar[0]];

    const tripleAbierto = [];
    tripleAbierto.push(datosSorteo.slice(-3));
    const arrayTripleAbierto = [...tripleAbierto[0]];

    const tripleGanador =
      arrayTripleAbierto[0] + arrayTripleAbierto[1] + arrayTripleAbierto[2];

    const combinacion1 =
      arrayCuadriculaValidar[2] +
      arrayCuadriculaValidar[4] +
      arrayCuadriculaValidar[6];
    const combinacion2 =
      arrayCuadriculaValidar[2] +
      arrayCuadriculaValidar[5] +
      arrayCuadriculaValidar[8];
    const combinacion3 =
      arrayCuadriculaValidar[1] +
      arrayCuadriculaValidar[4] +
      arrayCuadriculaValidar[7];
    const combinacion4 =
      arrayCuadriculaValidar[0] +
      arrayCuadriculaValidar[3] +
      arrayCuadriculaValidar[6];
    const combinacion5 =
      arrayCuadriculaValidar[0] +
      arrayCuadriculaValidar[4] +
      arrayCuadriculaValidar[8];
    const combinacion6 =
      arrayCuadriculaValidar[0] +
      arrayCuadriculaValidar[1] +
      arrayCuadriculaValidar[2];
    const combinacion7 =
      arrayCuadriculaValidar[3] +
      arrayCuadriculaValidar[4] +
      arrayCuadriculaValidar[5];
    const combinacion8 =
      arrayCuadriculaValidar[6] +
      arrayCuadriculaValidar[7] +
      arrayCuadriculaValidar[8];
    const combinacion9 =
      arrayCuadriculaValidar[6] +
      arrayCuadriculaValidar[4] +
      arrayCuadriculaValidar[2];

    setState({
      ...state,
      result: e,
      scan: false,
      ScanResult: true,
    });
    const case_ =
      datosSorteo.length === 23
        ? 'instantaneo'
        : datosSorteo.length === 41
        ? 'pasivo'
        : '';
    const isValor = datosSorteo && case_;

    // FECHA DEL SORTEO
    const fechaSorteo = datosSorteo.slice(3, 11);
    const dia = fechaSorteo.slice(0, 2);
    const mes = fechaSorteo.slice(2, 4);
    const anno = fechaSorteo.slice(4, 8);
    const fechaSorteoActual = anno + '-' + mes + '-' + dia;

    // FECHA DEL DIA DE HOY
    const date = ('0' + new Date().getDate()).slice(-2);
    const month = ('0' + (new Date().getMonth() + 1)).slice(-2);
    const year = new Date().getFullYear();
    const fechaActual = year + '-' + month + '-' + date;

    // RESTA ENTRE LAS FECHAS
    const fechaInicio = new Date(fechaSorteoActual).getTime();
    const fechaFin = new Date(fechaActual).getTime();
    const diff = fechaFin - fechaInicio;

    // console.log('difere', diff / (1000 * 60 * 60 * 24));

    const validateFecha = diff / (1000 * 60 * 60 * 24) <= 15;

    let stateObj = {};

    if (isValor && validateFecha) {
      stateObj = {
        ...state,
        ...(isValor ? {qr: datosSorteo} : {}),
      };
    } else if (!isValor && datosSorteo && datosSorteo.length === 8) {
        validacionSerialExterno(valor)
    }
      else if (!isValor) {
        validacionSerialInterno()
    }
    else {
      mensajeCaduco();
      salirEscaner();
    }

    const nSorteo = datosSorteo.slice(0, 3);

    if (validateFecha && case_ === 'instantaneo') {
      if (isValor && combinacion1.toString() === tripleGanador.toString()) {
        {
          nSorteo < '003' ? mensajeGanador() : mensajeNoGanador();
        }
      } else if (
        isValor &&
        combinacion2.toString() === tripleGanador.toString()
      ) {
        {
          nSorteo < '003' ? mensajeGanador2() : mensajeGanador8();
        }
      } else if (
        isValor &&
        combinacion3.toString() === tripleGanador.toString()
      ) {
        {
          nSorteo < '003' ? mensajeGanador3() : mensajeGanador7();
        }
      } else if (
        isValor &&
        combinacion4.toString() === tripleGanador.toString()
      ) {
        {
          nSorteo < '003' ? mensajeGanador4() : mensajeGanador6();
        }
      } else if (
        isValor &&
        combinacion5.toString() === tripleGanador.toString()
      ) {
        mensajeGanador5();
      } else if (
        isValor &&
        combinacion6.toString() === tripleGanador.toString()
      ) {
        {
          nSorteo < '003' ? mensajeGanador6() : mensajeGanador4();
        }
      } else if (
        isValor &&
        combinacion7.toString() === tripleGanador.toString()
      ) {
        {
          nSorteo < '003' ? mensajeGanador7() : mensajeGanador3();
        }
      } else if (
        isValor &&
        combinacion8.toString() === tripleGanador.toString()
      ) {
        {
          nSorteo < '003' ? mensajeGanador8() : mensajeGanador2();
        }
      } else if (
        isValor &&
        combinacion9.toString() === tripleGanador.toString()
      ) {
        {
          nSorteo >= '003' ? mensajeGanador() : mensajeNoGanador();
        }
      } else if (isValor) {
        mensajeNoGanador();
      }
    } else if (case_ === 'instantaneo') {
      mensajeCaduco();
      salirEscaner();
    }

    if (isValor)
      {
        salirEscaner({
          ...state,
          ...(isValor
            ? {
                qr: datosSorteo,
              }
            : {}),
        });
      }else{
        salirEscaner();
      }
  };

  const activeQR = () => {
    setState({
      ...state,
      scan: true,
    });
    setModalVisibility(true);
  };

  const scanAgain = () => {
    setState({
      ...state,
      scan: true,
      ScanResult: false,
      qr: '',
    });
    setModalVisibility(true);
  };

  const setModalVisibility = visible => {
    setVisibility(visible);
  };

  const mensajeCaduco = () => {
    Alert.alert('¡Lo sentimos!', 'Su ticket a caducado', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeGanador = () => {
    Alert.alert('¡Felicidades!', 'Eres ganador de un premio de 5$', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeGanador2 = () => {
    Alert.alert('¡Felicidades!', 'Eres ganador de un premio de 15$', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeGanador3 = () => {
    Alert.alert('¡Felicidades!', 'Eres ganador de un premio de 50$', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeGanador4 = () => {
    Alert.alert('¡Felicidades!', 'Eres ganador de un premio de 10$', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeGanador5 = () => {
    Alert.alert('¡Felicidades!', 'Eres ganador de un premio de 6.000$', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeGanador6 = () => {
    Alert.alert('¡Felicidades!', 'Eres ganador de un premio de 25$', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeGanador7 = () => {
    Alert.alert('¡Felicidades!', 'Eres ganador de un premio de 100$', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeGanador8 = () => {
    Alert.alert('¡Felicidades!', 'Eres ganador de un premio de 2.500$', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };
  const mensajeNoGanador = () => {
    Alert.alert('¡Lo sentimos!', 'No Eres ganador', [
      {text: 'OK', onPress: () => console.log('bien')},
    ]);
  };

  const validacionSerialExterno = valorExterno => {
    Alert.alert(
      'Triple Gordo informa',
      'Por favor contacte a su comprador comercial para validar premio. Su código de validación es:' +
        ' ' +
        valorExterno.qr,
      [
        {
          text: 'OK',
          onPress: () => setModalVisibility(!visibility),
        },
      ],
    );
  };

  const validacionSerialInterno = () => {
    Alert.alert(
      'Triple Gordo informa',
      'No se pudo leer correctamente el código QR por favor intente nuevamente',
      [
        {
          text: 'OK',
          onPress: () => setModalVisibility(!visibility),
        },
        // {
        //   text: 'Verificar Ticket',
        //   onPress: () => navigation.navigate('VerificarTicket'),
        // },
      ],
    );
  };

  const salirEscaner = (theState = {}) => {
    setState({
      ...state,
      ...theState,
      scan: false,
    });
    setModalVisibility(!visibility);
  };

  const makeSlideOutTranslation = (translationType, fromValue) => {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18,
      },
      to: {
        [translationType]: fromValue,
      },
    };
  };

  // const {scan, ScanResult} = this.state;
  const DeviceWidth = Dimensions.get('window').width;
  const tiraDatos = state.qr;

  //PAR MILLONARIO
  const parA = [];
  const parB = [];
  const valorParMillonario = tiraDatos.slice(0, 17).slice(-6);
  parA.push(valorParMillonario.slice(0, 3));
  parB.push(valorParMillonario.slice(3, 6));

  // TRIPLE TERMINAL
  const tripleTerm = [];
  const tripleTermB = [];
  const tripleTermC = [];
  const valorTripleTerm = tiraDatos.slice(17, 23).slice(-6);
  tripleTerm.push(valorTripleTerm.slice(0,2));
  tripleTermB.push(valorTripleTerm.slice(2,4));
  tripleTermC.push(valorTripleTerm.slice(4,6));

  // SUPER DUPLETA
  const s11 = [];
  s11.push(tiraDatos.slice(-18, -15));

  const s12 = [];
  s12.push(tiraDatos.slice(-15, -12));

  const s21 = [];
  s21.push(tiraDatos.slice(-12, -9));

  const s22 = [];
  s22.push(tiraDatos.slice(-9, -6));

  const s31 = [];
  s31.push(tiraDatos.slice(-6, -3));

  const s32 = [];
  s32.push(tiraDatos.slice(-3));

  const image = require('../assets/fondo_recortado.png');
  const imgPrincipal = require('../assets/fondoInicio.png');

  // CUADRICULA
  const cuadricula = [];
  cuadricula.push(tiraDatos.slice(-12, -3));
  const arrayCuadricula = tiraDatos.length === 23 ? [...cuadricula[0]] : [];

  // Triple
  const triple = [];
  triple.push(tiraDatos.slice(-3));
  const arrayTriple = tiraDatos.length === 23 ? [...triple[0]] : [];

  // NUMERO SORTEO
  const numSorteo = [];
  numSorteo.push(tiraDatos.slice(0, 3)); /* tiraDatos.slice(0, 3) */

  // DIA
  const diaSorteo = [];
  diaSorteo.push(tiraDatos.slice(3, 5));
  // MES
  const mesSorteo = [];
  mesSorteo.push(tiraDatos.slice(5, 7));
  // AÑO
  const annoSorteo = [];
  annoSorteo.push(tiraDatos.slice(7, 11));

  const iconScanColor = '#00000000';

  //PAR MILLONARIO

  return (
    <>
      <View>
        <Modal animationType={'slide'} transparent={false} visible={visibility}>
          <View style={(styles.modalContainer, styles.cardView)}>
            <View>
              {/* SCANER */}
              {state.scan && (
                <QRCodeScanner
                  reactivate={true}
                  onRead={e => onSucces(e)}
                  showMarker={true}
                  ref={node => {
                    state.scanner = node;
                  }}
                  cameraStyle={{height: SCREEN_HEIGHT}}
                  customMarker={
                    <View style={styles.rectangleContainer}>
                      <View style={styles.topOverlay}>
                        <Text
                          style={{
                            fontSize: 30,
                            color: 'white',
                            fontWeight: 'bold',
                          }}>
                          Escanear Código QR
                        </Text>
                      </View>

                      <View style={{flexDirection: 'row'}}>
                        <View style={styles.leftAndRightOverlay} />
                        <View style={styles.rectangle}>
                          <Icon
                            name="ios-qr-scanner"
                            size={SCREEN_WIDTH * 0.73}
                            color={iconScanColor}
                          />
                          <Animatable.View
                            style={styles.scanBar}
                            direction="alternate-reverse"
                            iterationCount="infinite"
                            duration={1700}
                            easing="linear"
                            animation={makeSlideOutTranslation(
                              'translateY',
                              SCREEN_WIDTH * -0.54,
                            )}
                          />
                        </View>

                        <View style={styles.leftAndRightOverlay} />
                      </View>

                      <View style={styles.bottomOverlay} />
                    </View>
                  }
                />
              )}
              <View style={styles.alignBoton}>
                <TouchableOpacity
                  onPress={() => salirEscaner()}
                  style={styles.btnSalir}>
                  <Text style={styles.buttonTextStyle}>Salir</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>

      {tiraDatos.length === 23 || tiraDatos.length === 41 ? <Header /> : <></>}

      {/* TEXTO */}
      {tiraDatos.length > 0 ? (
        <>
          {tiraDatos.length === 23 ? (
            <TituloText title="VERIFICACIÓN DE PREMIO INSTANTANEO" />
          ) : tiraDatos.length === 41 ? (
            <TituloText title="VERIFICACIÓN DE PREMIOS" />
          ) : (
            <></>
          )}
        </>
      ) : (
        <></>
      )}
      <View style={{flex: 1}}>
        {tiraDatos.length === 23 || tiraDatos.length === 41 ? (
          <Image
            source={image}
            resizeMode="cover"
            style={{
              position: 'absolute',
              left: 0,
              width: '100%',
              height: '100%',
              top: 0,
            }}
          />
        ) : (
          <Image
            source={imgPrincipal}
            resizeMode="cover"
            style={{
              position: 'absolute',
              left: 0,
              width: '100%',
              height: '100%',
              top: 0,
            }}
          />
        )}
        <ScrollView
          style={styles.scrollViewStyle}
          contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
          <View style={tiraDatos.length === 41 ? styles.separadorColor : styles.separadorSinColor}>
            {/* numero de sorteo y fecha */}
            {tiraDatos.length > 0 ? (
              <View style={{marginBottom: 5}}>
                <Text style={styles.textTitle1}>
                  Sorteo N° {numSorteo}   {diaSorteo}/{mesSorteo}/{annoSorteo}
                </Text>
              </View>
            ) : (
              <></>
            )}
          </View>
          {tiraDatos.length === 23 ? (
            <>
              {/* INSTANTANEO */}
              <View style={{paddingLeft: numSorteo[0] < '003' ? 0 : 20}}>
                {/* CUADRICULA */}
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: Platform.OS === 'android' ? 5 : 0,
                  }}>
                  <View style={styles.borderCuariculas}>
                    {/* INICIO COLUMNA 0 */}
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text
                          style={
                            Platform.OS === 'android'
                              ? styles.esitloMontos2
                              : styles.esitloMontos3
                          }>
                          6.000
                        </Text>
                        <Text
                          style={
                            Platform.OS === 'android'
                              ? styles.esitloMontos3
                              : styles.esitloMontos4
                          }>
                          DÓLARES
                        </Text>
                        <Image
                          style={styles.tinyLogoMontos}
                          source={require('../assets/6000.png')}
                        />
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.esitloMontos3}>
                          {numSorteo[0] < '003' ? '$ 25' : '$ 10'}
                        </Text>
                        <Text style={styles.esitloMontos4}>DÓLARES</Text>
                        <Image
                          style={styles.tinyLogoMontos}
                          source={require('../assets/flechaDerecha.png')}
                        />
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.esitloMontos3}>
                          {numSorteo[0] < '003' ? '$ 100' : '$ 50'}
                        </Text>
                        <Text style={styles.esitloMontos4}>DÓLARES</Text>
                        <Image
                          style={styles.tinyLogoMontos}
                          source={require('../assets/flechaDerecha.png')}
                        />
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.esitloMontos3}>
                          {numSorteo[0] < '003' ? '$ 2.500' : '$ 15'}
                        </Text>
                        <Text style={styles.esitloMontos4}>DÓLARES</Text>
                        <Image
                          style={styles.tinyLogoMontos}
                          source={require('../assets/flechaDerecha.png')}
                        />
                      </View>
                      {numSorteo[0] < '003' ? (
                        <></>
                      ) : (
                        <View
                          style={{
                            width: DeviceWidth * 0.19,
                            height: DeviceWidth * 0.19,
                            backgroundColor: 'transparent',
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center',
                          }}>
                          <Text
                            style={
                              Platform.OS === 'android'
                                ? styles.esitloMontos3
                                : styles.esitloMontos4
                            }>
                            Reintegro
                          </Text>
                          <Image
                            style={styles.tinyLogoMontos}
                            source={require('../assets/newArrow.png')}
                          />
                        </View>
                      )}
                    </View>
                    {/* INICIO COLUMNA 1 */}
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.esitloMontos3}>
                          {numSorteo[0] < '003' ? '$ 10' : '$ 25'}
                        </Text>
                        <Text style={styles.esitloMontos4}>DÓLARES</Text>
                        <Image
                          style={styles.tinyLogoMontos}
                          source={require('../assets/flechaAbajo.png')}
                        />
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          borderTopWidth: 4,
                          borderLeftWidth: 4,
                          borderRightWidth: 1,
                          borderBottomWidth: 1,
                          borderTopLeftRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[0]}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          borderLeftWidth: 4,
                          borderTopWidth: 1,
                          borderRightWidth: 1,
                          borderBottomWidth: 1,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[3]}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          borderBottomWidth: 4,
                          borderLeftWidth: 4,
                          borderTopWidth: 1,
                          borderRightWidth: 1,
                          borderBottomLeftRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[6]}
                        </Text>
                      </View>
                      {numSorteo[0] < '003' ? (
                        <></>
                      ) : (
                        <View
                          style={{
                            width: DeviceWidth * 0.19,
                            height: DeviceWidth * 0.15,
                            backgroundColor: 'transparent',
                          }}></View>
                      )}
                    </View>
                    {/* INICIO COLUMNA 2 */}
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.esitloMontos3}>
                          {numSorteo[0] < '003' ? '$ 50' : '$ 100'}
                        </Text>
                        <Text style={styles.esitloMontos4}>DÓLARES</Text>
                        <Image
                          style={styles.tinyLogoMontos}
                          source={require('../assets/flechaAbajo.png')}
                        />
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          borderTopWidth: 4,
                          borderBottomWidth: 1,
                          borderLeftWidth: 1,
                          borderRightWidth: 1,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[1]}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderTopWidth: 1,
                          borderBottomWidth: 1,
                          borderLeftWidth: 1,
                          borderRightWidth: 1,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[4]}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          borderBottomWidth: 4,
                          borderTopWidth: 1,
                          borderLeftWidth: 1,
                          borderRightWidth: 1,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[7]}
                        </Text>
                      </View>
                      {numSorteo[0] < '003' ? (
                        <></>
                      ) : (
                        <View
                          style={{
                            width: DeviceWidth * 0.19,
                            height: DeviceWidth * 0.15,
                            backgroundColor: 'transparent',
                          }}></View>
                      )}
                    </View>
                    {/* INICIO COLUMNA 3 */}
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.esitloMontos3}>
                          {numSorteo[0] < '003' ? '$ 15' : '$ 2.500'}
                        </Text>
                        <Text style={styles.esitloMontos4}>DÓLARES</Text>
                        <Image
                          style={styles.tinyLogoMontos}
                          source={require('../assets/flechaAbajo.png')}
                        />
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          borderTopWidth: 4,
                          borderRightWidth: 4,
                          borderBottomWidth: 1,
                          borderLeftWidth: 1,
                          borderTopRightRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[2]}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          borderTopWidth: 1,
                          borderRightWidth: 4,
                          borderBottomWidth: 1,
                          borderLeftWidth: 1,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[5]}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.19,
                          height: DeviceWidth * 0.15,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          borderBottomWidth: 4,
                          borderRightWidth: 4,
                          borderTopWidth: 1,
                          borderLeftWidth: 1,
                          borderBottomRightRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayCuadricula[8]}
                        </Text>
                      </View>
                      {numSorteo[0] < '003' ? (
                        <></>
                      ) : (
                        <View
                          style={{
                            width: DeviceWidth * 0.19,
                            height: DeviceWidth * 0.15,
                            backgroundColor: 'transparent',
                          }}></View>
                      )}
                    </View>
                    {/* INICIO COLUMNA 4 */}
                    <View>
                      {numSorteo && numSorteo[0] < '003' ? (
                        <View
                          style={{
                            width: DeviceWidth * 0.2,
                            height: DeviceWidth * 0.19,
                            backgroundColor: 'transparent',
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center',
                          }}>
                          <Text
                            style={
                              Platform.OS === 'android'
                                ? styles.esitloMontos3
                                : styles.esitloMontos4
                            }>
                            Reintegro
                          </Text>
                          <Image
                            style={styles.tinyLogoMontos}
                            source={require('../assets/flechaIzquierda.png')}
                          />
                        </View>
                      ) : (
                        <></>
                      )}
                      <View
                        style={{
                          width: DeviceWidth * 0.2,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                        }}></View>
                      <View
                        style={{
                          width: DeviceWidth * 0.2,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                        }}></View>
                      <View
                        style={{
                          width: DeviceWidth * 0.2,
                          height: DeviceWidth * 0.19,
                          backgroundColor: 'transparent',
                        }}></View>
                    </View>
                  </View>
                </View>
                {/* TRIPLE */}
                <View
                  style={
                    numSorteo[0] < '003'
                      ? styles.borderTripleInstantaneo
                      : styles.borderTripleInstantaneoNuevo
                  }>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.15,
                          height: DeviceWidth * 0.15,
                          marginBottom: 1,
                          backgroundColor: 'transparent',
                          borderColor: 'red',
                          borderBottomWidth: 4,
                          borderTopWidth: 4,
                          borderLeftWidth: 4,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayTriple[0]}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.15,
                          height: DeviceWidth * 0.15,
                          marginBottom: 1,
                          backgroundColor: 'transparent',
                          borderColor: 'red',
                          borderBottomWidth: 4,
                          borderTopWidth: 4,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayTriple[1]}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.15,
                          height: DeviceWidth * 0.15,
                          marginBottom: 1,
                          backgroundColor: 'transparent',
                          borderColor: 'red',
                          borderBottomWidth: 4,
                          borderRightWidth: 4,
                          borderTopWidth: 4,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>
                          {arrayTriple[2]}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </>
          ) : tiraDatos.length === 41 ? (
            <>
              {/* PAR MILLONARIO */}
              <View style={tiraDatos.length === 41 ? styles.separadorColor2 : ""}>
                <View style={styles.colorfondoTexto2}>
                  <Text style={styles.textTitle}>PAR MILLONARIO</Text>
                </View>
                <View style={styles.borderTriple}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    {/* TRIPLE A */}
                    <View style={{marginLeft:10, marginRight:10}}>
                      <View
                        style={{
                          width: DeviceWidth * 0.2,
                          height: DeviceWidth * 0.08,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={styles.esitloMontos5}>TRIPLE A</Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.2,
                          height: DeviceWidth * 0.12,
                          marginBottom: 1,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          // borderBottomWidth: 3,
                          // borderTopWidth: 3,
                          // borderLeftWidth: 3,
                          // borderRightWidth: 3,

                          // borderTopLeftRadius: 20,
                          // borderBottomLeftRadius: 20,
                          // borderTopRightRadius: 20,
                          // borderBottomRightRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>{parA}</Text>
                      </View>
                    </View>

                    {/* TRIPLE B */}
                    <View style={{marginLeft:10, marginRight:10}}>
                      <View
                        style={{
                          width: DeviceWidth * 0.2,
                          height: DeviceWidth * 0.08,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={styles.esitloMontos5}>TRIPLE B</Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.2,
                          height: DeviceWidth * 0.12,
                          marginBottom: 1,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          // borderBottomWidth: 3,
                          // borderTopWidth: 3,
                          // borderLeftWidth: 3,
                          // borderRightWidth: 3,

                          // borderTopLeftRadius: 20,
                          // borderBottomLeftRadius: 20,
                          // borderTopRightRadius: 20,
                          // borderBottomRightRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>{parB}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>

              {/* TRIPLE TERMINAL */}
              <View style={tiraDatos.length === 41 ? styles.separadorColor3 : ""}>
                <View style={styles.colorfondoTexto2}>
                  <Text style={styles.textTitle}>TRIPLE TERMINAL</Text>
                </View>
                <View style={styles.borderTriple}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.3,
                          height: DeviceWidth * 0.07,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={styles.esitloMontos5}>TERMINAL A</Text>
                      </View>
                    </View>
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.3,
                          height: DeviceWidth * 0.07,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={styles.esitloMontos5}>TERMINAL B</Text>
                      </View>
                    </View>
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.3,
                          height: DeviceWidth * 0.07,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={styles.esitloMontos5}>TERMINAL C</Text>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    {/* Terminal A */}
                      <View style={{marginLeft: 20, marginRight:20}}>
                        <View
                          style={{
                            width: DeviceWidth * 0.2,
                            height: DeviceWidth * 0.12,
                            marginBottom: 1,
                            backgroundColor: 'transparent',
                            borderColor: '#0a0d64',
                            // borderBottomWidth: 3,
                            // borderTopWidth: 3,
                            // borderLeftWidth: 3,
                            // borderRightWidth: 3,

                            // borderTopLeftRadius: 20,
                            // borderBottomLeftRadius: 20,
                            // borderTopRightRadius: 20,
                            // borderBottomRightRadius: 20,
                            flex: 1,
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center',
                          }}>
                          <Text style={styles.estiloNumeros}>
                            {tripleTerm}
                          </Text>
                        </View>
                      </View>
                      
                    {/* Terminal B */}
                      <View style={{marginLeft: 20, marginRight:20}}>
                        <View
                          style={{
                            width: DeviceWidth * 0.2,
                            height: DeviceWidth * 0.12,
                            marginBottom: 1,
                            backgroundColor: 'transparent',
                            borderColor: '#0a0d64',
                            // borderBottomWidth: 3,
                            // borderTopWidth: 3,
                            // borderLeftWidth: 3,
                            // borderRightWidth: 3,

                            // borderTopLeftRadius: 20,
                            // borderBottomLeftRadius: 20,
                            // borderTopRightRadius: 20,
                            // borderBottomRightRadius: 20,
                            flex: 1,
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center',
                          }}>
                          <Text style={styles.estiloNumeros}>
                            {tripleTermB}
                          </Text>
                        </View>
                      </View>

                    {/* Terminal C */}
                      <View style={{marginLeft: 20, marginRight:20}}>
                        <View
                          style={{
                            width: DeviceWidth * 0.2,
                            height: DeviceWidth * 0.12,
                            marginBottom: 1,
                            backgroundColor: 'transparent',
                            borderColor: '#0a0d64',
                            // borderBottomWidth: 3,
                            // borderTopWidth: 3,
                            // borderLeftWidth: 3,
                            // borderRightWidth: 3,

                            // borderTopLeftRadius: 20,
                            // borderBottomLeftRadius: 20,
                            // borderTopRightRadius: 20,
                            // borderBottomRightRadius: 20,
                            flex: 1,
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center',
                          }}>
                          <Text style={styles.estiloNumeros}>
                            {tripleTermC}
                          </Text>
                        </View>
                      </View>
                  </View>
                </View>
              </View>

              {/* SUPER DUPLETA */}
              <View style={{marginTop: 15, marginBottom: 10}}>
                <View style={styles.colorfondoTexto2}>
                  <Text style={styles.textTitle}>SÚPER DUPLETA</Text>
                </View>
                <View style={styles.borderTriple}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.3,
                          height: DeviceWidth * 0.07,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={styles.esitloMontos5}>TRIPLE 1</Text>
                      </View>
                    </View>
                    <View style={{marginLeft: 3}}>
                      <View
                        style={{
                          width: DeviceWidth * 0.3,
                          height: DeviceWidth * 0.07,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={styles.esitloMontos5}>TRIPLE 2</Text>
                      </View>
                    </View>
                    <View style={{marginLeft: 3}}>
                      <View
                        style={{
                          width: DeviceWidth * 0.3,
                          height: DeviceWidth * 0.07,
                          backgroundColor: 'transparent',
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'flex-end',
                        }}>
                        <Text style={styles.esitloMontos5}>TRIPLE 3</Text>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    {/* TRIPLE 1 */}
                    <View>
                      <View
                        style={{
                          width: DeviceWidth * 0.25,
                          height: DeviceWidth * 0.12,

                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          // borderRightWidth: 3,
                          // borderTopWidth: 3,
                          // borderLeftWidth: 3,
                          // borderTopLeftRadius: 20,
                          // borderTopRightRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>{s11}</Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.25,
                          height: DeviceWidth * 0.12,

                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          // borderBottomWidth: 3,
                          // borderRightWidth: 3,
                          // borderLeftWidth: 3,
                          // borderBottomRightRadius: 20,
                          // borderBottomLeftRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>{s12}</Text>
                      </View>
                    </View>

                    {/* TRIPLE 2 */}

                    <View style={{marginLeft: 15}}>
                      <View
                        style={{
                          width: DeviceWidth * 0.25,
                          height: DeviceWidth * 0.12,

                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          // borderRightWidth: 3,
                          // borderTopWidth: 3,
                          // borderLeftWidth: 3,
                          // borderTopLeftRadius: 20,
                          // borderTopRightRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>{s21}</Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.25,
                          height: DeviceWidth * 0.12,

                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          // borderBottomWidth: 3,
                          // borderRightWidth: 3,
                          // borderLeftWidth: 3,
                          // borderBottomRightRadius: 20,
                          // borderBottomLeftRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>{s22}</Text>
                      </View>
                    </View>

                    {/* TRIPLE 3 */}
                    <View style={{marginLeft: 15}}>
                      <View
                        style={{
                          width: DeviceWidth * 0.25,
                          height: DeviceWidth * 0.12,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          // borderRightWidth: 3,
                          // borderTopWidth: 3,
                          // borderLeftWidth: 3,
                          // borderTopLeftRadius: 20,
                          // borderTopRightRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>{s31}</Text>
                      </View>
                      <View
                        style={{
                          width: DeviceWidth * 0.25,
                          height: DeviceWidth * 0.12,
                          backgroundColor: 'transparent',
                          borderColor: '#0a0d64',
                          // borderBottomWidth: 3,
                          // borderRightWidth: 3,
                          // borderLeftWidth: 3,
                          // borderBottomRightRadius: 20,
                          // borderBottomLeftRadius: 20,
                          flex: 1,
                          alignItems: 'center',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.estiloNumeros}>{s32}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </>
          ) : (
            <>
              <View
                style={{
                  flex: 3,
                  justifyContent: 'flex-end',
                }}>
                <Text style={styles.textTitle2}>
                  Escanea el código QR y verifica los premios
                </Text>
              </View>
            </>
          )}

          {/* BOTÓN VALIDAR */}
          {!state.scan && !state.ScanResult && (
            <View
              style={
                tiraDatos.length === 23 || tiraDatos.length === 41
                  ? styles.alignBoton
                  : styles.alignBotonInicio
              }>
              <TouchableOpacity
                onPress={() => activeQR()}
                style={
                  tiraDatos.length === 23 || tiraDatos.length === 41
                    ? styles.buttonTouchable
                    : styles.buttonTouchableYellow
                }>
                <Text
                  style={
                    tiraDatos.length === 23 || tiraDatos.length === 41
                      ? styles.buttonTextStyle
                      : styles.buttonTextStyleBlue
                  }>
                  VALIDAR
                </Text>
              </TouchableOpacity>
            </View>
          )}
          {state.ScanResult && (
            <Fragment>
              <View
                style={
                  tiraDatos.length === 23 || tiraDatos.length === 41
                    ? styles.alignBoton
                    : styles.alignBotonInicio
                }>
                <TouchableOpacity
                  onPress={() => scanAgain()}
                  style={
                    tiraDatos.length === 23 || tiraDatos.length === 41
                      ? styles.buttonTouchable
                      : styles.buttonTouchableYellow
                  }>
                  <Text
                    style={
                      tiraDatos.length === 23 || tiraDatos.length === 41
                        ? styles.buttonTextStyle
                        : styles.buttonTextStyleBlue
                    }>
                    VALIDAR DE NUEVO!
                  </Text>
                </TouchableOpacity>
              </View>
            </Fragment>
          )}
          {/* </ImageBackground> */}
        </ScrollView>
      </View>
      {/* FOOTER */}
      <SocialNetworks />
    </>
  );
};

export default EscanearCodigo;
