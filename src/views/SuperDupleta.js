import {View, Text} from 'react-native';
import Footer from '../components/Footer/Footer';
import Header from '../components/Header/Header';
import TituloText from '../components/TituloText/TituloText';

const SuperDupleta = () => {
  return (
    <>
      <Header />
      <TituloText />
      <View>
        <Text>Super Dupleta</Text>
      </View>
      <Footer />
    </>
  );
};

export default SuperDupleta;