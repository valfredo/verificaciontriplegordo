import {View, Text} from 'react-native';
import Footer from '../components/Footer/Footer';
import Header from '../components/Header/Header';
import TituloText from '../components/TituloText/TituloText';

const ParMillonario = () => {
  return (
    <>
      <Header />
      <TituloText />
      <View>
        <Text>Par Millonario</Text>
      </View>
      <Footer />
    </>
  );
};

export default ParMillonario;
