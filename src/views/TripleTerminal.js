import {View, Text} from 'react-native';
import Footer from '../components/Footer/Footer';
import Header from '../components/Header/Header';
import TituloText from '../components/TituloText/TituloText';

const TripleTerminal = () => {
  return (
    <>
      <Header />
      <TituloText />
      <View>
        <Text>Triple Terminal</Text>
      </View>
      <Footer />
    </>
  );
};

export default TripleTerminal;