import React from 'react';
import {View, Image, Dimensions, TouchableOpacity} from 'react-native';
import styles from '../../assets/scanStyle';
const Footer = () => {
  const DeviceWidth = Dimensions.get('window').width;
  return (
    <View style={styles.footer}>
      <View style={styles.borderFooter}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.15,
                marginBottom: 1,
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('https://twitter.com/triplegordo_ofi')
                }>
                <Image
                  style={styles.tinyLogoRedes}
                  source={require('../../assets/twitter.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.15,
                marginBottom: 1,
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('https://www.facebook.com/TripleGordoV/')
                }>
                <Image
                  style={styles.tinyLogoRedes}
                  source={require('../../assets/facebook.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.15,
                marginBottom: 1,
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    'https://www.instagram.com/triplegordo_oficial/',
                  )
                }>
                <Image
                  style={styles.tinyLogoRedes}
                  source={require('../../assets/instagram.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Footer;
