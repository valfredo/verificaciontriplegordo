import React from 'react';
import { View, Text } from 'react-native';
import styles from '../../assets/scanStyle';

const TituloText = (props) => {
    return (
        <View style={styles.colorfondoTexto}>
          <Text style={styles.textTitle}>
            {props.title}
          </Text>
        </View>
    )
}

export default TituloText
