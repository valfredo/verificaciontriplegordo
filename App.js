import {SafeAreaView} from 'react-native-safe-area-context';
import MainStack from './src/navigation/MainStack';
import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
const App = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <NavigationContainer>
        <MainStack />
      </NavigationContainer>
    </SafeAreaView>
  );
};

export default App;
